import json
import urllib3
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
import contextlib
import OpenSSL.crypto
import requests
import tempfile

mopId= 'NWZiNTk0OGIyZGEwNTc1MWQ1NWNjN2I0'
brand='DS'
country='FR'
expected_result='final_approve'

update_result = {
     "final_approve": {"status":"Acceptation Definitive", "statusId": "4"},
    "temporary_approve": {"status":"Dossier Complet", "statusId": "3"},
    "refusal": {"status":"Refus Definitif", "statusId": "5"},
}

#1 get token
@contextlib.contextmanager
def pfx_to_pem(pfx_path, pfx_password):
    ''' Decrypts the .pfx file to be used with requests. '''
    with tempfile.NamedTemporaryFile(suffix='.pem') as t_pem:
        f_pem = open(t_pem.name, 'wb')
        pfx = open(pfx_path, 'rb').read()
        p12 = OpenSSL.crypto.load_pkcs12(pfx, pfx_password)
        f_pem.write(OpenSSL.crypto.dump_privatekey(OpenSSL.crypto.FILETYPE_PEM, p12.get_privatekey()))
        f_pem.write(OpenSSL.crypto.dump_certificate(OpenSSL.crypto.FILETYPE_PEM, p12.get_certificate()))
        ca = p12.get_ca_certificates()
        if ca is not None:
            for cert in ca:
                f_pem.write(OpenSSL.crypto.dump_certificate(OpenSSL.crypto.FILETYPE_PEM, cert))
        f_pem.close()
        yield t_pem.name

url = "https://monprojet-api-staging.sol.awsmpsa.com/oauth/v2/token"

payload = "{\n\t\"client_id\": \"summitdev\",\n\t\"client_secret\": \"NL47vYHE34t\",\n\t\"grant_type\": \"client_credentials\"\n}"
headers = {
  'Content-Type': 'application/json'
}
with pfx_to_pem('pfx/MDPSME11.pfx', 'jS&33f') as cert:
    response = requests.request("POST", url, headers=headers, data = payload,cert=cert ,verify=False)
body = json.loads(response.text)
token = body['access_token']

print('1. Mop Get auth token ->')
print(token)
print('*'*50)


#2 update mop
url = "https://monprojet-api-staging.sol.awsmpsa.com/api/mop/update"

payload="{\n   \"sourceName\":\"SUMMIT\",\n   \"mopId\":\""+mopId+"\",\n   \"brand\" : \""+brand+"\",\n   \"country\" : \""+country+"\",\n   " \
        "\"financeFile\": {\n   \t\t\"status\": \""+update_result[expected_result]['status']+"\",\n   \t\t\"statusId\": \""+update_result[expected_result]['statusId']+"\",\n   " \
        "\t\t\"preScore\": \"vert\"\n   }\n}"
headers = {
  'Content-Type': 'application/json',
  'Authorization': 'Bearer {}'.format(token)
}

with pfx_to_pem('pfx/MDPSME11.pfx', 'jS&33f') as cert:
    response = requests.request("POST", url, headers=headers, data = payload,cert=cert ,verify=False)

print('2. Mop update request ->')
print(payload)
print('Mop update response ->')
print(response.text)
print('*'*50)



#3 get mop
url = "https://monprojet-api-staging.sol.awsmpsa.com/api/mop/{}/{}/mopId/{}".format(brand,country,mopId)

payload={}
headers = {
  'Content-Type': 'application/json',
  'Authorization': 'Bearer {}'.format(token)
}
with pfx_to_pem('pfx/MDPSME11.pfx', 'jS&33f') as cert:
    response = requests.request("GET", url, headers=headers, data = payload,cert=cert ,verify=False)

print('3. Get mop response ->')
body = json.loads(response.text)
print('CallBackUrl-> {}'.format(body['callBackUrl']))
print('*'*50)
print(json.dumps(body, indent=True))


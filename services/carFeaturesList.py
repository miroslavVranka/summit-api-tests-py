import json

from props.properties import get_env, get_origin
from props.rest_controller import post_request


def post_car_features(env, brand, cntry, nameplate_spec_id, spec_id):
    url = get_env(env, brand, cntry) + "/car-features-list"
    payload = {
        "aggregationParams": {
            "levelAggregations": [
                {
                    "name": "engine",
                    "nesting": [
                        "engine",
                        "id"
                    ],
                    "children": []
                },
                {
                    "name": "exteriorColour",
                    "nesting": [
                        "exteriorColour",
                        "id"
                    ],
                    "children": [
                        {
                            "name": "filteredExteriorColour",
                            "nesting": [
                                "exteriorColour"
                            ],
                            "children": []
                        }
                    ]
                },
                {
                    "name": "interiorColour",
                    "nesting": [
                        "interiorColour",
                        "id"
                    ],
                    "children": [
                        {
                            "name": "filteredInteriorColour",
                            "nesting": [
                                "interiorColour"
                            ],
                            "children": []
                        }
                    ]
                }
            ],
            "relevancyAggregations": [
                {
                    "name": "engine",
                    "fields": [
                        "engine"
                    ],
                    "parent": "engine",
                    "operation": {
                        "size": 1
                    }
                },
                {
                    "name": "exteriorColour",
                    "fields": [
                        "exteriorColour"
                    ],
                    "parent": "exteriorColour",
                    "operation": {
                        "size": 1
                    }
                },
                {
                    "name": "exteriorColour",
                    "fields": [
                        "exteriorColour"
                    ],
                    "parent": "exteriorColour>filteredExteriorColour",
                    "operation": {
                        "size": 1
                    }
                },
                {
                    "name": "interiorColour",
                    "fields": [
                        "interiorColour"
                    ],
                    "parent": "interiorColour",
                    "operation": {
                        "size": 1
                    }
                },
                {
                    "name": "interiorColour",
                    "fields": [
                        "interiorColour"
                    ],
                    "parent": "interiorColour>filteredInteriorColour",
                    "operation": {
                        "size": 1
                    }
                }
            ]
        },
        "filters": [
            {
                "nesting": [
                    "nameplateBodyStyle"
                ],
                "name": "nameplateBodyStyle",
                "operator": "EQUALS",
                "value": nameplate_spec_id
            },
            {
                "nesting": [
                    "prices",
                    "monthlyPrices",
                    "amount"
                ],
                "name": "prices.monthlyPrices.amount",
                "operator": "BETWEEN",
                "value": {
                    "from": 0,
                    "to": 100000
                }
            },
            {
                "nesting": [
                    "prices",
                    "type"
                ],
                "name": "prices.type",
                "operator": "EQUALS",
                "value": "Employee"
            },
            {
                "nesting": [
                    "specPack",
                    "id"
                ],
                "name": "specPack.id",
                "operator": "EQUALS",
                "value": spec_id
            },
            {
                "nesting": [
                    "stock"
                ],
                "name": "stock",
                "operator": "EQUALS",
                "value": "false"
            }
        ]
    }

    response = post_request(url, payload)
    body = json.loads(response.text)
    engines = body['items'][0]['items']['engine']
    # interior = body['items'][1]['items']['interiorColour']
    if len(engines) > 1 :
        print("Number of engines => " + str(len(engines)))
    # if len(interior) > 1 :
    #     print("Number of interors => " + str(len(interior)))


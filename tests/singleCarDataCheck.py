from props.properties import get_config_page_url, get_trim_page_url
from services.carDetailItem import post_car_details_item
from services.carDetailsListAgregated import post_car_details_aggregated
from services.carNameplates import post_car_nameplates
from services.carOptionsList import post_car_options

instance = "sol"
brand = "AC"
env = "qa"
journey = "finance"

car_nameplates = post_car_nameplates(env, brand, instance, journey)
for nameplate in car_nameplates:
    print("_" * 90)
    car_details = post_car_details_aggregated(env, brand, instance, nameplate, journey)
    try:
        configUrl = get_config_page_url(env, brand, instance, journey, car_details['nameplateBodyStyleSlug'],
                              car_details['specPackSlug'],
                              car_details['engineGearboxFuelSlug'],
                              car_details['exteriorColourSlug'],
                              car_details['interiorColourSlug'], )
        trimUrl = get_trim_page_url(env, brand, instance, journey, car_details['nameplateBodyStyleSlug'])
        externalId = car_details['externalId']
        post_car_options(env, brand, instance, externalId, trimUrl, journey)
        post_car_details_item(env, brand, instance, externalId, configUrl, journey)
    except TypeError:
        print('Script stucked on TypeError')
        pass
    print("_"*90)


from props.properties import get_config_page_url, get_trim_page_url
from services.carDetailItem import post_car_details_item
from services.carDetailsListAgregated import post_car_details_aggregated
from services.carFeaturesList import post_car_features
from services.carNameplates import post_car_nameplates
from services.carOptionsList import post_car_options

country = "FR"
brand = "AC"
env = "preprod"
journey = "cash"

car_nameplates = post_car_nameplates(env, brand, country, journey)

for nameplate in car_nameplates:
    print("_" * 90)
    car_details = post_car_details_aggregated(env, brand, country, nameplate, journey)
    if type(car_details) is int:
        print('{} car returned status code {}'.format(nameplate, car_details))
    else:
        for car in car_details:
            # try:
            print(car['nameplateBodyStyleSlug'] +' ->> '+car['externalId'])
            print("_"*90)
            post_car_features(env, brand, country, car['nameplateBodyStyle'], car['specPackId'])
            # post_car_options(env, brand, country, car['externalId'], car['trimPageUrl'], journey)
            # post_car_details_item(env, brand, country, car['externalId'], car['configPageUrl'], journey)
            # except TypeError:
            #     print('Test failed on TypeError')
            #     pass
            print("_"*90)


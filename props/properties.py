env = {
    "qa": "qa",
    "dev": "dev",
    "preprod": "preprod",
    "prod": "prod",
}
brand = {
    "AC": "citroen",
    "AP": "peugeot",
    "DS": "ds",
    "OV": "vauxhall",
}
country = {
    'FR': 'fr',
    'GB': 'en'
}

def get_env(env_key, brand_key, country_key):
    if env_key == "prod" and country_key == "FR":
        return "https://store.{}.fr/spc-api/api/v1/fr/fr/{}".format(brand[brand_key], brand_key)
    elif env_key == "prod" and country_key == "GB":
        return "https://store.{}.co.uk/spc-api/api/v1/gb/en/{}".format(brand[brand_key], brand_key)
    elif brand_key == "OV" and country_key == "GB":
        return "https://"+env[env_key]+".vauxhall-uk-sol.psa-testing.summit-automotive.solutions/spc-api/api/v1/gb/en/" + brand_key.upper()
    elif country_key == "GB":
        return "https://psa-retail11-"+brand[brand_key]+"-" + env_key + ".summit-automotive.solutions/spc-api/api/v1/fr/fr/" + brand_key.upper()
    else:
        return "https://" + env_key + "."+brand[brand_key]+"-uk-sol.psa-testing.summit-automotive.solutions/spc-api/api/v1/gb/en/" + brand_key

def get_origin(env_key, brand_key, country_key):
    if country_key == "FR":
        return "https://psa-retail11-"+brand[brand_key]+"-" + env_key + ".summit-automotive.solutions/"
    else:
        return "https://" + env_key + "."+brand[brand_key]+"-uk-tests.psa-testing" + env_key + ".summit-automotive.solutions/"

def get_config_page_url(env_key, brand_key, country_key, journey, nameplate, spec_pack, engine, exterior, interior):
    return get_origin(env_key, brand_key,
               country_key) + "selector/configurable/" + journey + "/" + nameplate + "/" + spec_pack + "/" + engine + "/" + exterior + "/" + interior


def get_trim_page_url(env_key, brand_key, country_key, journey, nameplate):
    return get_origin(env_key, brand_key,
               country_key) + "trim/configurable/" + journey + "/" + nameplate + "/"


import random

import requests
import json

from props.properties import get_env, get_origin, get_config_page_url, get_trim_page_url, country
from props.rest_controller import post_request


def post_car_details_aggregated(env, brand, cntry, nameplate, journey):
    url = get_env(env, brand, cntry) + "/car-details-list"
    # payload = "{\"aggregationParams\":{\"levelAggregations\":[{\"name\":\"detailsAggregated\",\"nesting\":[\"detailsAggregated\"],\"children\":[]}],\"relevancyAggregations\":[{\"name\":\"prices.basePrice\",\"fields\":[\n  \"exteriorColour\",\n  \"exteriorColourSlug\",\n  \"engine\",\n  \"specPackSlug\",\n  \"specPack\",\n  \"engineGearboxFuelSlug\",\n  \"gearbox\",\n  \"prices\",\n  \"interiorColour\",\n  \"interiorColourSlug\",\n  \"externalId\",\n  \"nameplateBodyStyleSlug\",\n  \"nameplateBodyStyle\"\n],\"parent\":\"detailsAggregated\",\"operation\":{\"size\":1,\"sort\":\"recommended\"}}]},\"filters\":[{\"nesting\":[\"nameplateBodyStyleSlug\"],\"name\":\"nameplateBodyStyleSlug\",\"operator\":\"EQUALS\",\"value\":\""+nameplate+"\",\"parent\":null},{\"nesting\":[\"prices\",\"monthlyPrices\",\"amount\"],\"name\":\"prices.monthlyPrices.amount.global\",\"operator\":\"BETWEEN\",\"value\":{\"from\":1,\"to\":99999},\"parent\":null},{\"nesting\":[\"prices\",\"type\"],\"name\":\"prices.type\",\"operator\":\"EQUALS\",\"value\":\"Employee\"},{\"nesting\":[\"stock\"],\"name\":\"stock\",\"operator\":\"EQUALS\",\"value\":\"false\"}],\"extra\":{\"journey\":\""+journey+"\"}}"
    payload = {
      "aggregationParams": {
        "levelAggregations": [
          {
            "name": "detailsAggregated",
            "nesting": [
              "detailsAggregated"
            ],
            "children": [

            ]
          }
        ],
        "relevancyAggregations": [
          {
            "name": "prices.basePrice",
            "fields": [
              "exteriorColour",
              "exteriorColourSlug",
              "engine",
              "specPackSlug",
              "specPack",
              "engineGearboxFuelSlug",
              "gearbox",
              "prices",
              "interiorColour",
              "interiorColourSlug",
              "externalId",
              "nameplateBodyStyleSlug",
              "nameplateBodyStyle"
            ],
            "parent": "detailsAggregated",
            "operation": {
              "size": 1,
              "sort": "recommended"
            }
          }
        ]
      },
      "filters": [
        {
          "nesting": [
            "nameplateBodyStyleSlug"
          ],
          "name": "nameplateBodyStyleSlug",
          "operator": "EQUALS",
          "value": nameplate,
          "parent": None
        },
        {
          "nesting": [
            "prices",
            "monthlyPrices",
            "amount"
          ],
          "name": "prices.monthlyPrices.amount.global",
          "operator": "BETWEEN",
          "value": {
            "from": 1,
            "to": 99999
          },
          "parent": None
        },
        {
          "nesting": [
            "prices",
            "type"
          ],
          "name": "prices.type",
          "operator": "EQUALS",
          "value": "Employee"
        },
        {
          "nesting": [
            "stock"
          ],
          "name": "stock",
          "operator": "EQUALS",
          "value": "false"
        }
      ],
      "extra": {
        "journey": journey
      }
    }
    # payload = "{\"aggregationParams\":{\"levelAggregations\":[{\"name\":\"detailsAggregated\",\"nesting\":[\"detailsAggregated\"],\"children\":[]}],\"relevancyAggregations\":[{\"name\":\"prices.basePrice\",\"fields\":[\"exteriorColour\",\"exteriorColourSlug\",\"engine\",\"specPackSlug\",\"specPack\",\"engineGearboxFuelSlug\",\"gearbox\",\"prices\",\"interiorColour\",\"interiorColourSlug\",\"externalId\",\"nameplateBodyStyleSlug\",\"nameplateBodyStyle\"],\"parent\":\"detailsAggregated\",\"operation\":{\"size\":1,\"sort\":\"recommended\"}}]},\"filters\":[{\"nesting\":[\"nameplateBodyStyleSlug\"],\"name\":\"nameplateBodyStyleSlug\",\"operator\":\"EQUALS\",\"value\":\""+nameplate+"\",\"parent\":null},{\"nesting\":[\"prices\",\"monthlyPrices\",\"amount\"],\"name\":\"prices.monthlyPrices.amount.global\",\"operator\":\"BETWEEN\",\"value\":{\"from\":1,\"to\":99999},\"parent\":null},{\"nesting\":[\"prices\",\"type\"],\"name\":\"prices.type\",\"operator\":\"EQUALS\",\"value\":\"Employee\"},{\"nesting\":[\"stock\"],\"name\":\"stock\",\"operator\":\"EQUALS\",\"value\":\"false\"}],\"extra\":{\"journey\":\""+journey+"\"}}"
    response = post_request(url, payload)
    if response.status_code != 200:
       return response.status_code
    else:
        try:
            body = json.loads(response.text)
            pretty_json = None
            possibleOptions = []
            items = body['items'][0]['items']
            for a in items:
                for b in a['items']['prices.basePrice']:
                    for extColorItemPrice in b['exteriorColour']['pricesV2']:
                        for intColorItemPrice in b['interiorColour']['pricesV2']:
                            if extColorItemPrice['finalPriceInclTax'] == 0 and extColorItemPrice['type'] > "B2C_"+ journey.title() \
                                    and intColorItemPrice['type'] == "B2C_"+ journey.title() and intColorItemPrice[
                                'finalPriceInclTax'] > 0:
                                possibleOptions.append(
                                    {
                                        "externalId": b['externalId'],
                                        "nameplateBodyStyleSlug": b['nameplateBodyStyleSlug'],
                                        "specPackSlug": b['specPackSlug'],
                                        "specPackId": b['specPack']['id'],
                                        "nameplateBodyStyle": b['extraFields']['nameplateBodyStyle'],
                                        "engineId": b['engine']['id'],
                                        "engineGearboxFuelSlug": b['engineGearboxFuelSlug'],
                                        "exteriorColourSlug": b['exteriorColourSlug'],
                                        "exteriorColourId": b['exteriorColour']['id'],
                                        "exteriorColourFinalPriceInclTax": extColorItemPrice['finalPriceInclTax'],
                                        "interiorColourSlug": b['interiorColourSlug'],
                                        "interiorColourId": b['interiorColour']['id'],
                                        "interiorColourFinalPriceInclTax": intColorItemPrice['finalPriceInclTax'],
                                        "configPageUrl": get_config_page_url(env, brand, cntry, journey,
                                                                             b['nameplateBodyStyleSlug'],
                                                                             b['specPackSlug'],
                                                                             b['engineGearboxFuelSlug'],
                                                                             b['exteriorColourSlug'],
                                                                             b['interiorColourSlug']),
                                        "trimPageUrl": get_trim_page_url(env, brand, cntry, journey,
                                                                         b['nameplateBodyStyleSlug'])
                                    })
                        # print(extColorItemPrice)
            if len(possibleOptions) > 0:
                print("/car-details-list-aggregated test ok for -> " + nameplate)
                return possibleOptions
            else:
                print("/car-details-list-aggregated test nok for -> " + nameplate)
                return []

        except ValueError:
            print('Decoding JSON failed /wrong response from /car-nameplates')
            pass

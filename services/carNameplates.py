import json

import requests

from props.properties import get_env, get_origin
from props.rest_controller import post_request


def post_car_nameplates(env, brand, cntry, journey):
    url = get_env(env, brand, cntry) + "/car-nameplates"
    payload = {"aggregationParams": {
        "levelAggregations": [{"name": "nameplateBodyStyle", "nesting": ["nameplateBodyStyle"], "children": []}],
        "relevancyAggregations": [{"name": "prices.monthlyPrices.amount",
                                   "fields": ["id", "model", "prices", "images", "lcdv16", "bodyStyle", "nameplate",
                                              "grGearbox", "grBodyStyle", "nameplateBodyStyle", "promotionalText",
                                              "pricesV2", "regularPaymentConditions", "noDeposit", "offers", "specPack",
                                              "nameplateBodyStyleSlug", "exteriorColourSlug", "interiorColourSlug",
                                              "specPackSlug", "engineGearboxFuelSlug", "interiorColour", "exteriorColour",
                                              "engine", "fuel", "gearbox", "title", "isDefaultConfiguration",
                                              "defaultConfiguration", "externalId"], "parent": "nameplateBodyStyle",
                                   "operation": {"size": 1, "sort": "asc"}}]}, "filters": [
        {"nesting": ["prices", "type"], "name": "prices.basePrice", "operator": "EQUALS", "value": "Employee"},
        {"nesting": ["prices", "type"], "name": "prices.type", "operator": "EQUALS", "value": "Employee"},
        {"nesting": ["stock"], "name": "stock", "operator": "EQUALS", "value": "false"}], "extra": {"journey": journey}}

    response = post_request(url, payload)
    body = json.loads(response.text)
    items = body['items']
    nameplates = []
    for i in items:
        for cars in i['items']['prices.monthlyPrices.amount']:
            for p in cars['prices']:
                if p['type'] == 'Employee':
                    nameplates.append(cars['nameplateBodyStyleSlug'])
    for it in nameplates:
        print(it + " with index ->>>>> "+ str(nameplates.index(it)))
    return nameplates
    # nameplate = random.choice(nameplates) 01234567
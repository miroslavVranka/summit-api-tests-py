import requests

import uuid
user_token = uuid.uuid4().hex
# clientId = uuid.uuid4()
token = uuid.uuid4()
# csrf_token = uuid.uuid4().hex
clientId='a606de82-a90e-4732-aed4-30f8f19fa502'

def merge_two_dicts(x, y):
    z = x.copy()
    z.update(y)
    return z

# step 0 call index.php?route=common/saml/saml_start

url = "https://psa-retail11-citroen-qa.summit-automotive.solutions/digital/admin/index.php" \
      "?route=common/login"

payload = {}

headers = {
  'Origin': 'https://psa-retail11-citroen-qa.summit-automotive.solutions',
  'Upgrade-Insecure-Requests': '1',
  'Content-Type': 'multipart/form-data; boundary=----WebKitFormBoundaryXyAGd1QHTwsAwfVY',
  'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.80 Safari/537.36',
  'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3',
  'Cookie': 'SAUT_SESSION_AC_preprod=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJqd3QtaXNzdWVyIiwic3ViIjoic3NtLXVzZXItYXV0aCIsIm5iZiI6MTYwNDkyMjgzNCwiY3VzdG9tZXJfaWQiOm51bGwsInNlc3Npb25faWQiOiI3MTJlODNkZi0yMjgyLTExZWItODQ4Yi1jMzVkODg5NDRjYmYiLCJpYXQiOjE2MDQ5MjI4MzQsImp0aSI6IjcxMmU4M2RmLTIyODItMTFlYi04NDhiLWMzNWQ4ODk0NGNiZiJ9.yR8kZIya_tG_SeL0MtwAePAnDNid1iAktQZ_UJ3KpN8; __cfduid=d2f89d90a2ca5217f3a0e08d7ba6aea7b1605179591; SAUT_SESSION_AC_qa=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJqd3QtaXNzdWVyIiwic3ViIjoic3NtLXVzZXItYXV0aCIsIm5iZiI6MTYwNTE3OTU5MSwiY3VzdG9tZXJfaWQiOm51bGwsInNlc3Npb25faWQiOiI0YzFhZjgzMC0yNGQ4LTExZWItODQ4Yi1jMzVkODg5NDRjYmYiLCJpYXQiOjE2MDUxNzk1OTEsImp0aSI6IjRjMWFmODMwLTI0ZDgtMTFlYi04NDhiLWMzNWQ4ODk0NGNiZiJ9.ijp_tc4XK0IwbeRCkuaGhYsFAFTQRa-gwKh1edRhtsQ; SAUT_SESSION_AP_qa=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJqd3QtaXNzdWVyIiwic3ViIjoic3NtLXVzZXItYXV0aCIsIm5iZiI6MTYwNTIxNDAzMCwiY3VzdG9tZXJfaWQiOiI0MDYiLCJzZXNzaW9uX2lkIjoiNzU0NmNhYTMtMjUyOC0xMWViLTg0OGItYzM1ZDg4OTQ0Y2JmIiwiaWF0IjoxNjA1MjE0MDMwLCJqdGkiOiI3NTQ2Y2FhMy0yNTI4LTExZWItODQ4Yi1jMzVkODg5NDRjYmYifQ.d39qAvmL23RpMKhLPjcbAlugBjJsEvFHVzMt_G-P-Ko; OCSESSID=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJqd3QtaXNzdWVyIiwic3ViIjoic3NtLXVzZXItYXV0aCIsIm5iZiI6MTYwNTc3ODIwNSwiY3VzdG9tZXJfaWQiOm51bGwsInNlc3Npb25faWQiOiIyN2ZlOGIzNi0yYTRhLTExZWItODQ4Yi1jMzVkODg5NDRjYmYiLCJpYXQiOjE2MDU3NzgyMDUsImp0aSI6IjI3ZmU4YjM2LTJhNGEtMTFlYi04NDhiLWMzNWQ4ODk0NGNiZiJ9.smGKm2XKxPmch7TPeef84yTFW5w_xm23E0cTghKVHoU'
}
s = requests.Session()

response = s.request("POST", url, headers=headers, data = payload)

print('Request -> {}'.format(url))
print('Response -> {}'.format(response.status_code))
print('Headers -> {}'.format(response.headers))
cook = response.cookies
print(user_token)
print('Cookies -> {}'.format(response.cookies.get_dict()))
print("-"*50)


# step 1 call route=common/logout&user_token= with token

url = "https://psa-retail11-citroen-qa.summit-automotive.solutions/digital/admin/index.php" \
      "?route=common/logout&user_token={}".format(user_token)

payload = {}
headers = {
  'Origin': 'https://psa-retail11-citroen-qa.summit-automotive.solutions',
  'Upgrade-Insecure-Requests': '1',
  'Content-Type': 'multipart/form-data; boundary=----WebKitFormBoundaryXyAGd1QHTwsAwfVY',
  'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.80 Safari/537.36',
  'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3',
  'Cookie': 'SAUT_SESSION_AC_preprod=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJqd3QtaXNzdWVyIiwic3ViIjoic3NtLXVzZXItYXV0aCIsIm5iZiI6MTYwNDkyMjgzNCwiY3VzdG9tZXJfaWQiOm51bGwsInNlc3Npb25faWQiOiI3MTJlODNkZi0yMjgyLTExZWItODQ4Yi1jMzVkODg5NDRjYmYiLCJpYXQiOjE2MDQ5MjI4MzQsImp0aSI6IjcxMmU4M2RmLTIyODItMTFlYi04NDhiLWMzNWQ4ODk0NGNiZiJ9.yR8kZIya_tG_SeL0MtwAePAnDNid1iAktQZ_UJ3KpN8; __cfduid=d2f89d90a2ca5217f3a0e08d7ba6aea7b1605179591; SAUT_SESSION_AC_qa=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJqd3QtaXNzdWVyIiwic3ViIjoic3NtLXVzZXItYXV0aCIsIm5iZiI6MTYwNTE3OTU5MSwiY3VzdG9tZXJfaWQiOm51bGwsInNlc3Npb25faWQiOiI0YzFhZjgzMC0yNGQ4LTExZWItODQ4Yi1jMzVkODg5NDRjYmYiLCJpYXQiOjE2MDUxNzk1OTEsImp0aSI6IjRjMWFmODMwLTI0ZDgtMTFlYi04NDhiLWMzNWQ4ODk0NGNiZiJ9.ijp_tc4XK0IwbeRCkuaGhYsFAFTQRa-gwKh1edRhtsQ; SAUT_SESSION_AP_qa=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJqd3QtaXNzdWVyIiwic3ViIjoic3NtLXVzZXItYXV0aCIsIm5iZiI6MTYwNTIxNDAzMCwiY3VzdG9tZXJfaWQiOiI0MDYiLCJzZXNzaW9uX2lkIjoiNzU0NmNhYTMtMjUyOC0xMWViLTg0OGItYzM1ZDg4OTQ0Y2JmIiwiaWF0IjoxNjA1MjE0MDMwLCJqdGkiOiI3NTQ2Y2FhMy0yNTI4LTExZWItODQ4Yi1jMzVkODg5NDRjYmYifQ.d39qAvmL23RpMKhLPjcbAlugBjJsEvFHVzMt_G-P-Ko; OCSESSID=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJqd3QtaXNzdWVyIiwic3ViIjoic3NtLXVzZXItYXV0aCIsIm5iZiI6MTYwNTc3ODIwNSwiY3VzdG9tZXJfaWQiOm51bGwsInNlc3Npb25faWQiOiIyN2ZlOGIzNi0yYTRhLTExZWItODQ4Yi1jMzVkODg5NDRjYmYiLCJpYXQiOjE2MDU3NzgyMDUsImp0aSI6IjI3ZmU4YjM2LTJhNGEtMTFlYi04NDhiLWMzNWQ4ODk0NGNiZiJ9.smGKm2XKxPmch7TPeef84yTFW5w_xm23E0cTghKVHoU'
}

response = requests.request("GET", url, headers=headers, data = payload)


print('Request -> {}'.format(url))
print('Response -> {}'.format(response.status_code))
# print('Response -> {}'.format(response.text))
print('Headers -> {}'.format(response.headers))
print('Cookies -> {}'.format(response.cookies))
print("-"*50)


# step 2 call common/dashboard&user_token=

print(user_token)
url = "https://psa-retail11-citroen-qa.summit-automotive.solutions/digital/admin/index.php" \
      "?route=common/dashboard&user_token={}".format(user_token)

payload = {}
headers = {
  'Origin': 'https://psa-retail11-citroen-qa.summit-automotive.solutions',
  'Upgrade-Insecure-Requests': '1',
  'Content-Type': 'multipart/form-data; boundary=----WebKitFormBoundaryZEKiClahpqN0mGiR',
  'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.80 Safari/537.36',
  'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3',
  'Cookie': 'SAUT_SESSION_AC_preprod=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJqd3QtaXNzdWVyIiwic3ViIjoic3NtLXVzZXItYXV0aCIsIm5iZiI6MTYwNDkyMjgzNCwiY3VzdG9tZXJfaWQiOm51bGwsInNlc3Npb25faWQiOiI3MTJlODNkZi0yMjgyLTExZWItODQ4Yi1jMzVkODg5NDRjYmYiLCJpYXQiOjE2MDQ5MjI4MzQsImp0aSI6IjcxMmU4M2RmLTIyODItMTFlYi04NDhiLWMzNWQ4ODk0NGNiZiJ9.yR8kZIya_tG_SeL0MtwAePAnDNid1iAktQZ_UJ3KpN8; __cfduid=d2f89d90a2ca5217f3a0e08d7ba6aea7b1605179591; SAUT_SESSION_AC_qa=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJqd3QtaXNzdWVyIiwic3ViIjoic3NtLXVzZXItYXV0aCIsIm5iZiI6MTYwNTE3OTU5MSwiY3VzdG9tZXJfaWQiOm51bGwsInNlc3Npb25faWQiOiI0YzFhZjgzMC0yNGQ4LTExZWItODQ4Yi1jMzVkODg5NDRjYmYiLCJpYXQiOjE2MDUxNzk1OTEsImp0aSI6IjRjMWFmODMwLTI0ZDgtMTFlYi04NDhiLWMzNWQ4ODk0NGNiZiJ9.ijp_tc4XK0IwbeRCkuaGhYsFAFTQRa-gwKh1edRhtsQ; SAUT_SESSION_AP_qa=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJqd3QtaXNzdWVyIiwic3ViIjoic3NtLXVzZXItYXV0aCIsIm5iZiI6MTYwNTIxNDAzMCwiY3VzdG9tZXJfaWQiOiI0MDYiLCJzZXNzaW9uX2lkIjoiNzU0NmNhYTMtMjUyOC0xMWViLTg0OGItYzM1ZDg4OTQ0Y2JmIiwiaWF0IjoxNjA1MjE0MDMwLCJqdGkiOiI3NTQ2Y2FhMy0yNTI4LTExZWItODQ4Yi1jMzVkODg5NDRjYmYifQ.d39qAvmL23RpMKhLPjcbAlugBjJsEvFHVzMt_G-P-Ko; OCSESSID=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJqd3QtaXNzdWVyIiwic3ViIjoic3NtLXVzZXItYXV0aCIsIm5iZiI6MTYwNTc3ODIwNSwiY3VzdG9tZXJfaWQiOm51bGwsInNlc3Npb25faWQiOiIyN2ZlOGIzNi0yYTRhLTExZWItODQ4Yi1jMzVkODg5NDRjYmYiLCJpYXQiOjE2MDU3NzgyMDUsImp0aSI6IjI3ZmU4YjM2LTJhNGEtMTFlYi04NDhiLWMzNWQ4ODk0NGNiZiJ9.smGKm2XKxPmch7TPeef84yTFW5w_xm23E0cTghKVHoU'
}

response = requests.request("GET", url, headers=headers, data = payload)

print('Request -> {}'.format(url))
print('Response -> {}'.format(response.status_code))
# print('Response -> {}'.format(response.text))
print('Headers -> {}'.format(response.headers))
print('Cookies -> {}'.format(response.cookies))
print("-"*50)




# step 3 call common/dashboard&user_token=

url = "https://psa-retail-citroen-qa.summit-automotive.solutions/cj/api/user"

payload = {}
headers = {
  'Accept': 'application/json',
  'Origin': 'https://psa-retail11-citroen-qa.summit-automotive.solutions',
  # 'X-AUTH-OC-TOKEN': 'Bearer hxhusr4ZGePdJiIrIlrXUAvjeEvzK1GR',
  'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.80 Safari/537.36',
  # 'X-AUTH-OC-SESS-ID': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJqd3QtaXNzdWVyIiwic3ViIjoic3NtLXVzZXItYXV0aCIsIm5iZiI6MTYwNTgxNTk4OSwiY3VzdG9tZXJfaWQiOm51bGwsInNlc3Npb25faWQiOiIyMmY1OWU3ZS0yYWEyLTExZWItODQ4Yi1jMzVkODg5NDRjYmYiLCJpYXQiOjE2MDU4MTU5ODksImp0aSI6IjIyZjU5ZTdlLTJhYTItMTFlYi04NDhiLWMzNWQ4ODk0NGNiZiJ9.vH4V5DTwR3DHC5SmLfV78BCwaqMbhvpx6F-Y4wpKYh4',
  # 'Cookie': 'SAUT_SESSION_AC_preprod=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJqd3QtaXNzdWVyIiwic3ViIjoic3NtLXVzZXItYXV0aCIsIm5iZiI6MTYwNDkyMjgzNCwiY3VzdG9tZXJfaWQiOm51bGwsInNlc3Npb25faWQiOiI3MTJlODNkZi0yMjgyLTExZWItODQ4Yi1jMzVkODg5NDRjYmYiLCJpYXQiOjE2MDQ5MjI4MzQsImp0aSI6IjcxMmU4M2RmLTIyODItMTFlYi04NDhiLWMzNWQ4ODk0NGNiZiJ9.yR8kZIya_tG_SeL0MtwAePAnDNid1iAktQZ_UJ3KpN8; __cfduid=d2f89d90a2ca5217f3a0e08d7ba6aea7b1605179591; SAUT_SESSION_AC_qa=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJqd3QtaXNzdWVyIiwic3ViIjoic3NtLXVzZXItYXV0aCIsIm5iZiI6MTYwNTE3OTU5MSwiY3VzdG9tZXJfaWQiOm51bGwsInNlc3Npb25faWQiOiI0YzFhZjgzMC0yNGQ4LTExZWItODQ4Yi1jMzVkODg5NDRjYmYiLCJpYXQiOjE2MDUxNzk1OTEsImp0aSI6IjRjMWFmODMwLTI0ZDgtMTFlYi04NDhiLWMzNWQ4ODk0NGNiZiJ9.ijp_tc4XK0IwbeRCkuaGhYsFAFTQRa-gwKh1edRhtsQ; SAUT_SESSION_AP_qa=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJqd3QtaXNzdWVyIiwic3ViIjoic3NtLXVzZXItYXV0aCIsIm5iZiI6MTYwNTIxNDAzMCwiY3VzdG9tZXJfaWQiOiI0MDYiLCJzZXNzaW9uX2lkIjoiNzU0NmNhYTMtMjUyOC0xMWViLTg0OGItYzM1ZDg4OTQ0Y2JmIiwiaWF0IjoxNjA1MjE0MDMwLCJqdGkiOiI3NTQ2Y2FhMy0yNTI4LTExZWItODQ4Yi1jMzVkODg5NDRjYmYifQ.d39qAvmL23RpMKhLPjcbAlugBjJsEvFHVzMt_G-P-Ko; OCSESSID=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJqd3QtaXNzdWVyIiwic3ViIjoic3NtLXVzZXItYXV0aCIsIm5iZiI6MTYwNTc3ODIwNSwiY3VzdG9tZXJfaWQiOm51bGwsInNlc3Npb25faWQiOiIyN2ZlOGIzNi0yYTRhLTExZWItODQ4Yi1jMzVkODg5NDRjYmYiLCJpYXQiOjE2MDU3NzgyMDUsImp0aSI6IjI3ZmU4YjM2LTJhNGEtMTFlYi04NDhiLWMzNWQ4ODk0NGNiZiJ9.smGKm2XKxPmch7TPeef84yTFW5w_xm23E0cTghKVHoU'
}

response = requests.request("GET", url, headers=headers, data = payload)

print('Request -> {}'.format(url))
print('Response -> {}'.format(response.status_code))
# print('Response -> {}'.format(response.text))
print('Headers -> {}'.format(response.headers))
print('Cookies -> {}'.format(response.cookies))
print("-"*50)




import json
import uuid

import requests


user_token = uuid.uuid4().hex
# clientId = uuid.uuid4()
token = uuid.uuid4()

# 1 
url = "https://ppr-api-psa-exchange.shakazoola.com/v1/estimate/" \
      "init/7866f93e-c390-4cda-9111-d790233b5fd2/fr/Citroen?lcdv=1CLEA5RESFB0A010&useCase=estimate&carType=VN&co2=0&ncPrice=38800&ignorePromo=1&"

payload = {}
headers = {
  'Accept': 'application/json, text/javascript, */*; q=0.01',
  'Origin': 'https://psa-retail11-citroen-qa.summit-automotive.solutions',
  'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_16_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.80 Safari/537.36',
  'Content-Type': 'application/x-www-form-urlencoded'
}

response = requests.request("POST", url, headers=headers, data = payload)
body = json.loads(response.text)
pxId = str(body['data']['valuation']['id'])
valuationKey = body['data']['valuation']['valuationKey']

print('Request -> {}'.format(url))
print('Response -> {}'.format(response.status_code))
print('Response body -> {}'.format(json.dumps(body,indent=None)))
print('Headers -> {}'.format(response.headers))
print("-"*100)
print("-"*100)


# 2
url = "https://ppr-api-psa-exchange.shakazoola.com/v1/estimate/" \
      "version/7866f93e-c390-4cda-9111-d790233b5fd2/fr/Citroen?action=init"

payload = "regNumber=FE121BE&overallMileage=12%E2%80%AF100&valuation={\"id\":"+pxId+",\"chronoId\":null,\"creationAt\":\"2020-11-23 20:45:18\",\"updatedAt\":null,\"statusId\":null,\"statusLabel\":null,\"mopInd\":null,\"manuel\":1,\"excluded\":null,\"widgetKey\":\"7866f93e-c390-4cda-9111-d790233b5fd2\",\"lastStepVisited\":\"estimate-init\",\"brand\":\"AC\",\"country\":\"FR\",\"mopId\":null,\"newProjectCar\":\"1CLEA5RESFB0A010\",\"desiredValuationDate\":null,\"valuationKey\":\""+valuationKey+"\",\"template\":\"Citroen\",\"declarativeSheet\":null,\"carType\":\"VN\",\"carCategory\":null,\"energy\":null,\"co2\":\"0\",\"image\":\"\",\"commercial\":\"\",\"cta\":null,\"conversionPremium\":null,\"promotions\":null,\"oldPromotions\":null,\"governmentContribution\":null,\"taxable\":null,\"distance\":null,\"incomeRf\":null,\"test\":false,\"bonusAmount\":0,\"bonusDetails\":\"[]\",\"governmentContributionAmount\":0,\"CarDetails\":{\"id\":"+pxId+",\"regNumber\":\"\",\"vin\":false,\"firstRegistration\":null,\"dateGrayCard\":null,\"makeId\":null,\"makeName\":null,\"modelId\":null,\"modelName\":null,\"carType\":null,\"registrationMonth\":null,\"registrationYear\":null,\"fuelTypeId\":null,\"fuelTypeName\":null,\"bodyTypeId\":null,\"bodyTypeName\":null,\"doors\":null,\"gearBoxId\":null,\"gearBoxName\":null,\"dinPower\":null,\"horsePower\":null,\"versionId\":null,\"versionName\":null,\"segmentId\":null,\"liter\":null,\"finish\":null,\"finishVersion\":null,\"CarState\":{\"id\":"+pxId+",\"overallMileage\":null,\"annualMileage\":null,\"maxMileage\":null,\"expectedMileage\":null,\"state\":null,\"registrationCertificateName\":null,\"pledged\":null,\"roll\":null,\"import\":null,\"technicalControl\":null,\"interiorCondition\":null,\"mechanicalDamage\":null,\"electronicDamage\":null,\"serviceHistory\":null,\"frontTiresCondition\":null,\"rearTiresCondition\":null}},\"PersonalDetails\":{\"id\":"+pxId+",\"civility\":false,\"firstName\":false,\"lastName\":false,\"email\":false,\"phoneNumber\":false,\"homePhone\":false,\"language\":\"fr\",\"Address\":{\"id\":"+pxId+",\"zipCode\":false,\"address1\":false,\"address2\":false,\"city\":false},\"Agreements\":{\"id\":"+pxId+",\"sharePersonalInfo\":null,\"termsAgreed\":null,\"refusalContact\":null,\"refusalPhoneCall\":null,\"refusalEmail\":null},\"Dealer\":{\"id\":"+pxId+",\"name\":false,\"email\":false,\"phone\":false,\"zipCode\":false,\"city\":false,\"sitegeo\":null,\"RRDICode\":null,\"latitude\":null,\"longitude\":null,\"siretNumber\":null,\"emailVN\":false,\"emailVO\":false,\"TVANumber\":false,\"country\":null,\"region\":null,\"addressFacturation\":false,\"addressDelivery\":false,\"brand\":null,\"group\":null,\"PersonList\":{\"civility\":null,\"firstName\":null,\"lastName\":null,\"codePosition\":null,\"labelPosition\":null,\"email\":null}},\"DeliveryDealer\":{\"id\":"+pxId+",\"name\":false,\"email\":false,\"phone\":false,\"zipCode\":false,\"city\":false,\"sitegeo\":null,\"RRDICode\":null,\"latitude\":null,\"longitude\":null,\"siretNumber\":null,\"emailVN\":false,\"emailVO\":false,\"TVANumber\":false,\"country\":null,\"region\":null,\"addressFacturation\":false,\"addressDelivery\":false,\"brand\":null,\"group\":null,\"PersonList\":{\"civility\":null,\"firstName\":null,\"lastName\":null,\"codePosition\":null,\"labelPosition\":null,\"email\":null}}},\"FeedbackAnswer\":{\"id\":"+pxId+",\"question\":\"Vous n'Ãªtes pas intÃ©ressÃ©e par l'offre de reprise car \",\"answer\":null},\"legalNotice\":{\"noticeEngagementFirstPart\":\"\",\"noticeEngagementSecondPart\":\"\",\"noticeEstimateFirstPart\":\"\",\"noticeEstimateSecondPart\":\"\"},\"Valuation\":{\"id\":"+pxId+",\"profile\":null,\"validQuotation\":null,\"bcaQuotation\":null,\"autobizMarket\":null,\"engagementValuationDate\":null,\"engagementValidUntil\":null,\"engagementQuotation\":null,\"engagementValuationApplied\":null,\"frevoStat\":null,\"frevoAccessorie\":null,\"frevoTires\":null,\"frevoInterior\":null,\"frevoMaintenance\":null,\"frevoDamage\":null,\"frevoReal\":null,\"engagementAccepted\":null,\"estimatedAutobizMarket\":null,\"estimateValuationDate\":null,\"estimateValuationValidUntil\":null,\"estimatedQuotation\":null,\"estimateValuationApplied\":null,\"updated\":null,\"estimateAccepted\":null,\"estimatedFrevoStat\":null,\"BrandContribution\":{\"id\":"+pxId+",\"vin\":false,\"adminstrativeFileVin\":false,\"carNum\":null,\"administrativePower\":null,\"versionId\":null,\"versionLabel\":\"\",\"derivedModelId\":null,\"derivedModelLabel\":null,\"isAddedToBasket\":null,\"addedToBasketDate\":null,\"availabilityDate\":null,\"PriceVN\":null,\"amount\":null}}}"
headers = {
  'Accept': 'application/json, text/javascript, */*; q=0.01',
  'Origin': 'https://psa-retail11-citroen-qa.summit-automotive.solutions',
  'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_16_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.80 Safari/537.36',
  'Content-Type': 'application/x-www-form-urlencoded'
}

response = requests.request("POST", url, headers=headers, data = payload)

print('Request -> {}'.format(url))
print('Response -> {}'.format(response.status_code))
print('Response body -> {}'.format(json.dumps(body,indent=None)))
print('Headers -> {}'.format(response.headers))
print("-"*100)
print("-"*100)

# 3
url = "https://ppr-api-psa-exchange.shakazoola.com/v1/estimate/" \
      "yourValuation/7866f93e-c390-4cda-9111-d790233b5fd2/fr/Citroen"

payload = "version=43878&valuation={\"id\":"+pxId+",\"chronoId\":null,\"creationAt\":\"2020-11-23 20:45:18\",\"updatedAt\":null,\"statusId\":null,\"statusLabel\":null,\"mopInd\":null,\"manuel\":1,\"excluded\":null,\"widgetKey\":\"7866f93e-c390-4cda-9111-d790233b5fd2\",\"lastStepVisited\":\"estimate-version\",\"brand\":\"AC\",\"country\":\"FR\",\"mopId\":null,\"newProjectCar\":\"1CLEA5RESFB0A010\",\"desiredValuationDate\":null,\"valuationKey\":\""+valuationKey+"\",\"template\":\"Citroen\",\"declarativeSheet\":null,\"carType\":\"VN\",\"carCategory\":null,\"energy\":null,\"co2\":\"0\",\"image\":\"\",\"commercial\":\"\",\"cta\":null,\"conversionPremium\":null,\"promotions\":null,\"oldPromotions\":null,\"governmentContribution\":null,\"taxable\":null,\"distance\":null,\"incomeRf\":null,\"test\":true,\"bonusAmount\":0,\"bonusDetails\":\"[]\",\"governmentContributionAmount\":0,\"CarDetails\":{\"id\":"+pxId+",\"regNumber\":\"FE121BE\",\"vin\":\"WVWZZZ1KZ4W068129\",\"firstRegistration\":\"2004-04-01\",\"dateGrayCard\":\"2020-08-01\",\"makeId\":92,\"makeName\":\"VOLKSWAGEN\",\"modelId\":335,\"modelName\":\"GOLF\",\"carType\":\"PARTICULAR\",\"registrationMonth\":\"04\",\"registrationYear\":\"2004\",\"fuelTypeId\":1,\"fuelTypeName\":\"ESSENCE\",\"bodyTypeId\":2,\"bodyTypeName\":\"BERLINE\",\"doors\":5,\"gearBoxId\":1,\"gearBoxName\":\"MANUELLE\",\"dinPower\":75,\"horsePower\":5,\"versionId\":null,\"versionName\":null,\"segmentId\":null,\"liter\":1.4,\"finish\":null,\"finishVersion\":\"BASE,ASCOTT,CONFORT,CONFORT PLUS,EDITION,SPORT,TREND,TREND PACK,SPECIAL,PACIFIC,CHAMP,COMFORT LINE,OCEAN,SPIRIT,OXFORD,TURIJN,OPTIVE,B2B 30-YEARS,RABBIT,SYDNEY,25-ANOS,FRESH,GTI,MINT,OPEN,SE,WHITE ARCTIC,YEARS,MATCH,ALLOY,GTI S,FAMILY,FIRST,BONJOVI,CHROME,CE,EXTRA,LAST,CL,GT,JUBILEUM,NORMAL\",\"CarState\":{\"id\":"+pxId+",\"overallMileage\":\"12100\",\"annualMileage\":null,\"maxMileage\":null,\"expectedMileage\":null,\"state\":null,\"registrationCertificateName\":null,\"pledged\":null,\"roll\":null,\"import\":null,\"technicalControl\":null,\"interiorCondition\":null,\"mechanicalDamage\":null,\"electronicDamage\":null,\"serviceHistory\":null,\"frontTiresCondition\":null,\"rearTiresCondition\":null}},\"PersonalDetails\":{\"id\":"+pxId+",\"civility\":false,\"firstName\":false,\"lastName\":false,\"email\":false,\"phoneNumber\":false,\"homePhone\":false,\"language\":\"fr\",\"Address\":{\"id\":"+pxId+",\"zipCode\":false,\"address1\":false,\"address2\":false,\"city\":false},\"Agreements\":{\"id\":"+pxId+",\"sharePersonalInfo\":null,\"termsAgreed\":null,\"refusalContact\":null,\"refusalPhoneCall\":null,\"refusalEmail\":null},\"Dealer\":{\"id\":"+pxId+",\"name\":false,\"email\":false,\"phone\":false,\"zipCode\":false,\"city\":false,\"sitegeo\":null,\"RRDICode\":null,\"latitude\":null,\"longitude\":null,\"siretNumber\":null,\"emailVN\":false,\"emailVO\":false,\"TVANumber\":false,\"country\":null,\"region\":null,\"addressFacturation\":false,\"addressDelivery\":false,\"brand\":null,\"group\":null,\"PersonList\":{\"civility\":null,\"firstName\":null,\"lastName\":null,\"codePosition\":null,\"labelPosition\":null,\"email\":null}},\"DeliveryDealer\":{\"id\":"+pxId+",\"name\":false,\"email\":false,\"phone\":false,\"zipCode\":false,\"city\":false,\"sitegeo\":null,\"RRDICode\":null,\"latitude\":null,\"longitude\":null,\"siretNumber\":null,\"emailVN\":false,\"emailVO\":false,\"TVANumber\":false,\"country\":null,\"region\":null,\"addressFacturation\":false,\"addressDelivery\":false,\"brand\":null,\"group\":null,\"PersonList\":{\"civility\":null,\"firstName\":null,\"lastName\":null,\"codePosition\":null,\"labelPosition\":null,\"email\":null}}},\"FeedbackAnswer\":{\"id\":"+pxId+",\"question\":\"Vous n'Ãªtes pas intÃ©ressÃ©e par l'offre de reprise car \",\"answer\":null},\"legalNotice\":{\"noticeEngagementFirstPart\":\"\",\"noticeEngagementSecondPart\":\"\",\"noticeEstimateFirstPart\":\"\",\"noticeEstimateSecondPart\":\"\"},\"Valuation\":{\"id\":"+pxId+",\"profile\":null,\"validQuotation\":null,\"bcaQuotation\":null,\"autobizMarket\":null,\"engagementValuationDate\":null,\"engagementValidUntil\":null,\"engagementQuotation\":null,\"engagementValuationApplied\":null,\"frevoStat\":null,\"frevoAccessorie\":null,\"frevoTires\":null,\"frevoInterior\":null,\"frevoMaintenance\":null,\"frevoDamage\":null,\"frevoReal\":null,\"engagementAccepted\":null,\"estimatedAutobizMarket\":null,\"estimateValuationDate\":null,\"estimateValuationValidUntil\":null,\"estimatedQuotation\":null,\"estimateValuationApplied\":null,\"updated\":null,\"estimateAccepted\":null,\"estimatedFrevoStat\":null,\"BrandContribution\":{\"id\":"+pxId+",\"vin\":false,\"adminstrativeFileVin\":false,\"carNum\":null,\"administrativePower\":null,\"versionId\":null,\"versionLabel\":\"\",\"derivedModelId\":null,\"derivedModelLabel\":null,\"isAddedToBasket\":null,\"addedToBasketDate\":null,\"availabilityDate\":null,\"PriceVN\":null,\"amount\":null}},\"CarDamage\":{\"elementId\":null,\"elementName\":null,\"damageId\":null,\"damageName\":null,\"costs\":null}}"
headers = {
  'Accept': 'application/json, text/javascript, */*; q=0.01',
  'Origin': 'https://psa-retail11-citroen-qa.summit-automotive.solutions',
  'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_16_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.80 Safari/537.36',
  'Content-Type': 'application/x-www-form-urlencoded'
}

response = requests.request("POST", url, headers=headers, data = payload)

print('Request -> {}'.format(url))
print('Response -> {}'.format(response.status_code))
print('Response body -> {}'.format(json.dumps(body,indent=None)))
print('Headers -> {}'.format(response.headers))
print("-"*100)
print("-"*100)

# 4
url = "https://ppr-api-psa-exchange.shakazoola.com/v1/estimate/valuation-accepted/" \
      "7866f93e-c390-4cda-9111-d790233b5fd2/fr/Citroen"

payload = "&valuation={\"id\":"+pxId+",\"chronoId\":null,\"creationAt\":\"2020-11-23 20:45:18\",\"updatedAt\":null,\"statusId\":100,\"statusLabel\":\"Estimation donnÃ©e au client\",\"mopInd\":null,\"manuel\":1,\"excluded\":null,\"widgetKey\":\"7866f93e-c390-4cda-9111-d790233b5fd2\",\"lastStepVisited\":\"estimate-your-valuation\",\"brand\":\"AC\",\"country\":\"FR\",\"mopId\":null,\"newProjectCar\":\"1CLEA5RESFB0A010\",\"desiredValuationDate\":null,\"valuationKey\":\""+valuationKey+"\",\"template\":\"Citroen\",\"declarativeSheet\":null,\"carType\":\"VN\",\"carCategory\":null,\"energy\":null,\"co2\":\"0\",\"image\":\"\",\"commercial\":\"\",\"cta\":null,\"conversionPremium\":null,\"promotions\":null,\"oldPromotions\":null,\"governmentContribution\":null,\"taxable\":null,\"distance\":null,\"incomeRf\":null,\"test\":true,\"bonusAmount\":0,\"bonusDetails\":\"[]\",\"governmentContributionAmount\":0,\"CarDetails\":{\"id\":"+pxId+",\"regNumber\":\"FE121BE\",\"vin\":\"WVWZZZ1KZ4W068129\",\"firstRegistration\":\"2004-04-01\",\"dateGrayCard\":\"2020-08-01\",\"makeId\":92,\"makeName\":\"VOLKSWAGEN\",\"modelId\":335,\"modelName\":\"GOLF\",\"carType\":\"PARTICULAR\",\"registrationMonth\":4,\"registrationYear\":2004,\"fuelTypeId\":1,\"fuelTypeName\":\"ESSENCE\",\"bodyTypeId\":2,\"bodyTypeName\":\"BERLINE\",\"doors\":5,\"gearBoxId\":1,\"gearBoxName\":\"MANUELLE\",\"dinPower\":75,\"horsePower\":5,\"versionId\":43878,\"versionName\":\"1.4 75 CONFORT\",\"segmentId\":2,\"liter\":1.4,\"finish\":\"CONFORT\",\"finishVersion\":\"BASE,ASCOTT,CONFORT,CONFORT PLUS,EDITION,SPORT,TREND,TREND PACK,SPECIAL,PACIFIC,CHAMP,COMFORT LINE,OCEAN,SPIRIT,OXFORD,TURIJN,OPTIVE,B2B 30-YEARS,RABBIT,SYDNEY,25-ANOS,FRESH,GTI,MINT,OPEN,SE,WHITE ARCTIC,YEARS,MATCH,ALLOY,GTI S,FAMILY,FIRST,BONJOVI,CHROME,CE,EXTRA,LAST,CL,GT,JUBILEUM,NORMAL\",\"CarState\":{\"id\":"+pxId+",\"overallMileage\":12100,\"annualMileage\":null,\"maxMileage\":null,\"expectedMileage\":null,\"state\":null,\"registrationCertificateName\":null,\"pledged\":null,\"roll\":null,\"import\":null,\"technicalControl\":null,\"interiorCondition\":null,\"mechanicalDamage\":null,\"electronicDamage\":null,\"serviceHistory\":null,\"frontTiresCondition\":null,\"rearTiresCondition\":null}},\"PersonalDetails\":{\"id\":"+pxId+",\"civility\":false,\"firstName\":false,\"lastName\":false,\"email\":false,\"phoneNumber\":false,\"homePhone\":false,\"language\":\"fr\",\"Address\":{\"id\":"+pxId+",\"zipCode\":false,\"address1\":false,\"address2\":false,\"city\":false},\"Agreements\":{\"id\":"+pxId+",\"sharePersonalInfo\":null,\"termsAgreed\":null,\"refusalContact\":null,\"refusalPhoneCall\":null,\"refusalEmail\":null},\"Dealer\":{\"id\":"+pxId+",\"name\":false,\"email\":false,\"phone\":false,\"zipCode\":false,\"city\":false,\"sitegeo\":null,\"RRDICode\":null,\"latitude\":null,\"longitude\":null,\"siretNumber\":null,\"emailVN\":false,\"emailVO\":false,\"TVANumber\":false,\"country\":null,\"region\":null,\"addressFacturation\":false,\"addressDelivery\":false,\"brand\":null,\"group\":null,\"PersonList\":{\"civility\":null,\"firstName\":null,\"lastName\":null,\"codePosition\":null,\"labelPosition\":null,\"email\":null}},\"DeliveryDealer\":{\"id\":"+pxId+",\"name\":false,\"email\":false,\"phone\":false,\"zipCode\":false,\"city\":false,\"sitegeo\":null,\"RRDICode\":null,\"latitude\":null,\"longitude\":null,\"siretNumber\":null,\"emailVN\":false,\"emailVO\":false,\"TVANumber\":false,\"country\":null,\"region\":null,\"addressFacturation\":false,\"addressDelivery\":false,\"brand\":null,\"group\":null,\"PersonList\":{\"civility\":null,\"firstName\":null,\"lastName\":null,\"codePosition\":null,\"labelPosition\":null,\"email\":null}}},\"FeedbackAnswer\":{\"id\":"+pxId+",\"question\":\"Vous n'Ãªtes pas intÃ©ressÃ©e par l'offre de reprise car \",\"answer\":null},\"legalNotice\":{\"noticeEngagementFirstPart\":\"\",\"noticeEngagementSecondPart\":\"\",\"noticeEstimateFirstPart\":\"<p> (1) Avec 650 â¬ de frais de remise en Ã©tat standard dÃ©jÃ  dÃ©duit. Frais Ã  ajuster selon l'Ã©tat rÃ©el de votre vÃ©hicule. Estimation fournie Ã  titre indicatif, selon les conditions gÃ©nÃ©rales d'utilisation du site. </p>\",\"noticeEstimateSecondPart\":\"<p> (2) 0 Â de bonus reprise. Offre non cumulable rÃ©servÃ©e aux particuliers, valable jusqu'au 23/11/2020 pour toute commande sur store.citroen.fr, du CitroÃ«n   sÃ©lectionnÃ©, livrÃ© avant le 23/12/2020 .</p>\"},\"Valuation\":{\"id\":"+pxId+",\"profile\":\"B2C\",\"validQuotation\":true,\"bcaQuotation\":null,\"autobizMarket\":null,\"engagementValuationDate\":null,\"engagementValidUntil\":null,\"engagementQuotation\":null,\"engagementValuationApplied\":null,\"frevoStat\":null,\"frevoAccessorie\":null,\"frevoTires\":null,\"frevoInterior\":null,\"frevoMaintenance\":null,\"frevoDamage\":null,\"frevoReal\":null,\"engagementAccepted\":null,\"estimatedAutobizMarket\":3700,\"estimateValuationDate\":\"2020-11-23 20:45:49\",\"estimateValuationValidUntil\":\"2020-12-07 20:45:49\",\"estimatedQuotation\":2500,\"estimateValuationApplied\":1850,\"updated\":null,\"estimateAccepted\":null,\"estimatedFrevoStat\":650,\"BrandContribution\":{\"id\":"+pxId+",\"vin\":false,\"adminstrativeFileVin\":false,\"carNum\":null,\"administrativePower\":null,\"versionId\":null,\"versionLabel\":\"\",\"derivedModelId\":null,\"derivedModelLabel\":null,\"isAddedToBasket\":null,\"addedToBasketDate\":null,\"availabilityDate\":null,\"PriceVN\":null,\"amount\":null}},\"CarDamage\":{\"elementId\":null,\"elementName\":null,\"damageId\":null,\"damageName\":null,\"costs\":null}}"
headers = {
  'Accept': 'application/json, text/javascript, */*; q=0.01',
  'Origin': 'https://psa-retail11-citroen-qa.summit-automotive.solutions',
  'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_16_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.80 Safari/537.36',
  'Content-Type': 'application/x-www-form-urlencoded'
}

response = requests.request("POST", url, headers=headers, data = payload)

print('Request -> {}'.format(url))
print('Response -> {}'.format(response.status_code))
print('Response body -> {}'.format(json.dumps(body,indent=None)))
print('Headers -> {}'.format(response.headers))
print("-"*100)
print("-"*100)

# 5
url = "https://psa-retail11-citroen-qa.summit-automotive.solutions/px/api/fr/fr/AC/create/" \
      "{}?clearSession=false".format(valuationKey)

payload = "{}"
headers = {
  'Accept': 'application/json',
  'X-Auth-Token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJqd3QtaXNzdWVyIiwic3ViIjoic3NtLXVzZXItYXV0aCIsIm5iZiI6MTYwNjE0NDIwOSwiY3VzdG9tZXJfaWQiOiIyNDIiLCJzZXNzaW9uX2lkIjoiNWNhZmRiZmYtMmQ5ZS0xMWViLTg0OGItYzM1ZDg4OTQ0Y2JmIiwiaWF0IjoxNjA2MTQ0MjA5LCJqdGkiOiI1Y2FmZGJmZi0yZDllLTExZWItODQ4Yi1jMzVkODg5NDRjYmYifQ.F2xkm4jMkXRwKcgWGa3uL89t50wKbpwsKP5Ld_pELvI',
  'x-auth-px': '9xwh5123',
  'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_16_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.80 Safari/537.36',
  'Origin': 'https://psa-retail11-citroen-qa.summit-automotive.solutions',
  'Content-Type': 'application/json',
  # 'Cookie': 'SAUT_SESSION_AC_preprod=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJqd3QtaXNzdWVyIiwic3ViIjoic3NtLXVzZXItYXV0aCIsIm5iZiI6MTYwNDkyMjgzNCwiY3VzdG9tZXJfaWQiOm51bGwsInNlc3Npb25faWQiOiI3MTJlODNkZi0yMjgyLTExZWItODQ4Yi1jMzVkODg5NDRjYmYiLCJpYXQiOjE2MDQ5MjI4MzQsImp0aSI6IjcxMmU4M2RmLTIyODItMTFlYi04NDhiLWMzNWQ4ODk0NGNiZiJ9.yR8kZIya_tG_SeL0MtwAePAnDNid1iAktQZ_UJ3KpN8; __cfduid=d2f89d90a2ca5217f3a0e08d7ba6aea7b1605179591; SAUT_SESSION_AC_qa=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJqd3QtaXNzdWVyIiwic3ViIjoic3NtLXVzZXItYXV0aCIsIm5iZiI6MTYwNTE3OTU5MSwiY3VzdG9tZXJfaWQiOm51bGwsInNlc3Npb25faWQiOiI0YzFhZjgzMC0yNGQ4LTExZWItODQ4Yi1jMzVkODg5NDRjYmYiLCJpYXQiOjE2MDUxNzk1OTEsImp0aSI6IjRjMWFmODMwLTI0ZDgtMTFlYi04NDhiLWMzNWQ4ODk0NGNiZiJ9.ijp_tc4XK0IwbeRCkuaGhYsFAFTQRa-gwKh1edRhtsQ; SAUT_SESSION_AP_qa=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJqd3QtaXNzdWVyIiwic3ViIjoic3NtLXVzZXItYXV0aCIsIm5iZiI6MTYwNTIxNDAzMCwiY3VzdG9tZXJfaWQiOiI0MDYiLCJzZXNzaW9uX2lkIjoiNzU0NmNhYTMtMjUyOC0xMWViLTg0OGItYzM1ZDg4OTQ0Y2JmIiwiaWF0IjoxNjA1MjE0MDMwLCJqdGkiOiI3NTQ2Y2FhMy0yNTI4LTExZWItODQ4Yi1jMzVkODg5NDRjYmYifQ.d39qAvmL23RpMKhLPjcbAlugBjJsEvFHVzMt_G-P-Ko'
}

response = requests.request("POST", url, headers=headers, data = payload)

print('Request -> {}'.format(url))
print('Response -> {}'.format(response.status_code))
print('Response body -> {}'.format(response.text))
print('Headers -> {}'.format(response.headers))
print("-"*100)
print("-"*100)

# 6

url = "https://psa-retail11-citroen-qa.summit-automotive.solutions/px/api/fr/fr/AC/" \
      "widget-params?co2=0&lcdv=1CLEA5RESFB0A010&ncPrice=38800&ignorePromo=1&evaluateToken=true&noUpdate=false"

payload = {}
headers = {
  'Accept': 'application/json',
  # 'X-Auth-Token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJqd3QtaXNzdWVyIiwic3ViIjoic3NtLXVzZXItYXV0aCIsIm5iZiI6MTYwNjEyMjY0NywiY3VzdG9tZXJfaWQiOm51bGwsInNlc3Npb25faWQiOiIyZWUwYmUxOS0yZDZjLTExZWItODQ4Yi1jMzVkODg5NDRjYmYiLCJpYXQiOjE2MDYxMjI2NDcsImp0aSI6IjJlZTBiZTE5LTJkNmMtMTFlYi04NDhiLWMzNWQ4ODk0NGNiZiJ9.EOvat0krbCzgVtrLfkr6Q4gRX8QoLWMjFAb0a7zUNqE',
  'x-auth-px': '9xwh5123',
  'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_16_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.80 Safari/537.36',
  # 'Cookie': 'SAUT_SESSION_AC_preprod=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJqd3QtaXNzdWVyIiwic3ViIjoic3NtLXVzZXItYXV0aCIsIm5iZiI6MTYwNDkyMjgzNCwiY3VzdG9tZXJfaWQiOm51bGwsInNlc3Npb25faWQiOiI3MTJlODNkZi0yMjgyLTExZWItODQ4Yi1jMzVkODg5NDRjYmYiLCJpYXQiOjE2MDQ5MjI4MzQsImp0aSI6IjcxMmU4M2RmLTIyODItMTFlYi04NDhiLWMzNWQ4ODk0NGNiZiJ9.yR8kZIya_tG_SeL0MtwAePAnDNid1iAktQZ_UJ3KpN8; __cfduid=d2f89d90a2ca5217f3a0e08d7ba6aea7b1605179591; SAUT_SESSION_AC_qa=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJqd3QtaXNzdWVyIiwic3ViIjoic3NtLXVzZXItYXV0aCIsIm5iZiI6MTYwNTE3OTU5MSwiY3VzdG9tZXJfaWQiOm51bGwsInNlc3Npb25faWQiOiI0YzFhZjgzMC0yNGQ4LTExZWItODQ4Yi1jMzVkODg5NDRjYmYiLCJpYXQiOjE2MDUxNzk1OTEsImp0aSI6IjRjMWFmODMwLTI0ZDgtMTFlYi04NDhiLWMzNWQ4ODk0NGNiZiJ9.ijp_tc4XK0IwbeRCkuaGhYsFAFTQRa-gwKh1edRhtsQ; SAUT_SESSION_AP_qa=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJqd3QtaXNzdWVyIiwic3ViIjoic3NtLXVzZXItYXV0aCIsIm5iZiI6MTYwNTIxNDAzMCwiY3VzdG9tZXJfaWQiOiI0MDYiLCJzZXNzaW9uX2lkIjoiNzU0NmNhYTMtMjUyOC0xMWViLTg0OGItYzM1ZDg4OTQ0Y2JmIiwiaWF0IjoxNjA1MjE0MDMwLCJqdGkiOiI3NTQ2Y2FhMy0yNTI4LTExZWItODQ4Yi1jMzVkODg5NDRjYmYifQ.d39qAvmL23RpMKhLPjcbAlugBjJsEvFHVzMt_G-P-Ko'
}
print('Request -> {}'.format(url))
print('Response -> {}'.format(response.status_code))
print('Response body -> {}'.format(response.text))
print('Headers -> {}'.format(response.headers))
print("-"*100)
print("-"*100)
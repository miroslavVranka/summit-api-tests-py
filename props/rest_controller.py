import requests

def post_request(url, payload):
    try:
        if type(payload) is str:
            response = requests.request("POST", url, data=payload)
        else:
            response = requests.request("POST", url, json=payload)
        # print("Request -> ")
        # print(payload)
        print("-"*50)
        print('Response of ' + url[59:] + ' -> ' + str(response.status_code))
        # print(response.text)
        # size = response.headers.get('content-length')
        # print(size)
        # print size in megabytes
        # print('\t{:<40}: {:.2f} MB'.format('FILE SIZE', int(size) / float(1 << 20)))
        return response
    except requests.exceptions.HTTPError as errh:
        print("Http Error:", errh)
    except requests.exceptions.ConnectionError as errc:
        print("Error Connecting:", errc)
    except requests.exceptions.Timeout as errt:
        print("Timeout Error:", errt)
    except requests.exceptions.RequestException as err:
        print("OOps: Something Else", err)

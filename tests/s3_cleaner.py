import boto3

def delete_all_versions(bucket_name: str, prefix: str):
    session = boto3.session.Session()
    s3 = session.resource("s3")
    bucket = s3.Bucket(bucket_name)
    if prefix is None:
        print('Removed {}'.format(bucket_name))
        bucket.object_versions.delete()
    else:
        bucket.object_versions.filter(Prefix=prefix).delete()

buckets = [
    # 'drivvn-ac-fr-integration',
    #        'drivvn-ac-fr-regress',
    #        'drivvn-ac-fr-smoke',
    #        'drivvn-ac-gb-integration',
    #        'drivvn-ac-gb-regress'
           'drivvn-ds-gb-regress'
           # 'drivvn-ac-gb-smoke',
           # 'drivvn-ap-fr-integration',
           # 'drivvn-ap-fr-regress',
           # 'drivvn-ap-fr-smoke',
           # 'drivvn-ds-fr-regress',
           # 'drivvn-ds-fr-smoke',
           # 'drivvn-ov-gb-integration',
           # 'drivvn-ov-gb-regress',
           # 'drivvn-ov-gb-smoke'
]
for i in buckets:
    delete_all_versions(i, None)
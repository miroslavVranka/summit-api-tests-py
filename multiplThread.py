def run_deals_threads(thread_count: int):
    threads = []
    for i in range(0, thread_count):
        process = Thread(target=remove_deals)
        process.start()
        threads.append(process)

    for process in threads:
        print(process)
        process.join()

run_deals_threads(2)

import requests

import uuid
state = (uuid.uuid4().hex)*5
# clientId = uuid.uuid4()
naky_code = uuid.uuid4()
# csrf_token = uuid.uuid4().hex
clientId='a606de82-a90e-4732-aed4-30f8f19fa502'

def merge_two_dicts(x, y):
    z = x.copy()
    z.update(y)
    return z

# step 0 call psa-retail11-citroen-qa.summit-automotive.solutions/configurable/finance/ and retrieve __cfduid
url = "https://psa-retail11-citroen-qa.summit-automotive.solutions/configurable/finance/"

payload = {}
headers = {
  'Upgrade-Insecure-Requests': '1',
  'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.80 Safari/537.36',
  'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3',
  # 'Cookie': '__cfduid=d9f2bd9aa5aecc156f7b85929b64f7ca21602574395; SAUT_SESSION_AC_preprod=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJqd3QtaXNzdWVyIiwic3ViIjoic3NtLXVzZXItYXV0aCIsIm5iZiI6MTYwNDkyMjgzNCwiY3VzdG9tZXJfaWQiOm51bGwsInNlc3Npb25faWQiOiI3MTJlODNkZi0yMjgyLTExZWItODQ4Yi1jMzVkODg5NDRjYmYiLCJpYXQiOjE2MDQ5MjI4MzQsImp0aSI6IjcxMmU4M2RmLTIyODItMTFlYi04NDhiLWMzNWQ4ODk0NGNiZiJ9.yR8kZIya_tG_SeL0MtwAePAnDNid1iAktQZ_UJ3KpN8; SAUT_SESSION_AC_qa=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJqd3QtaXNzdWVyIiwic3ViIjoic3NtLXVzZXItYXV0aCIsIm5iZiI6MTYwNTA0MDI3MCwiY3VzdG9tZXJfaWQiOm51bGwsInNlc3Npb25faWQiOiJlM2I2NThiOS0yMzkzLTExZWItODQ4Yi1jMzVkODg5NDRjYmYiLCJpYXQiOjE2MDUwNDAyNzAsImp0aSI6ImUzYjY1OGI5LTIzOTMtMTFlYi04NDhiLWMzNWQ4ODk0NGNiZiJ9.RhprMtbPQ9-OtEs0M8gITv84D9goet_SfFRbnDxRv-c'
}
s = requests.Session()

response = s.request("GET", url, headers=headers, data = payload, auth=('summit', 'summit'))
c1 = response.cookies.get_dict()
print('Request -> {}'.format(url))
print('Response -> {}'.format(response.status_code))
print('Headers -> {}'.format(response.headers))
print('Cookies -> {}'.format(c1))
print("-"*50)


# step 1 call /digital-api/api/customers/is-logged and retrieve SAUT_SESSION_AP_qa
url = "https://psa-retail11-citroen-qa.summit-automotive.solutions/digital-api/api/customers/is-logged"

payload = {}
headers = {
  'Accept': 'application/json, text/plain, */*',
  'x-market': 'fr',
  'x-language': 'fr',
  'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.80 Safari/537.36',
  'x-context': 'ec41_b2b',
}

response = s.request("GET", url, headers=headers, data = payload)
c2 = response.cookies.get_dict()
final_cookie = merge_two_dicts(c1,c2)
print('Request -> {}'.format(url))
print('Response -> {}'.format(response.status_code))
print('Headers -> {}'.format(response.headers))
print('Cookies -> {}'.format(c2))
print("-"*50)


# step 2 (open brandidLogin)
url = "https://id-dcr-pre.citroen.com/login?openid.claimed_id=" \
      "http://specs.openid.net/auth/2.0/identifier_select&openid.identity=" \
      "http://specs.openid.net/auth/2.0/identifier_select&openid.mode=checkid_setup&openid.ns=" \
      "http://specs.openid.net/auth/2.0&newcvs=1&goto=" \
      "https://idpcvs-preprod.citroen.com/am/oauth2/authorize?client_id={}&response_type=code&scope=openid%20implied_consent%20profile&redirect_uri=" \
      "https://psa-retail11-citroen-qa.summit-automotive.solutions/digital-api/brandid/login&realm=clientsB2CCitroen&locale=fr-FR&state={}".format(clientId,state)
payload = {}
headers = {
  'Upgrade-Insecure-Requests': '1',
  'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.80 Safari/537.36',
  'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3',
}

response = s.request("GET", url, headers=headers, data = payload)
cook = response.cookies.get_dict()
csrf_token = cook['DCROPENIDAC']
print('Request -> {}'.format(url))
print('Response -> {}'.format(response.status_code))
print('Headers -> {}'.format(response.headers))
print("-"*50)


# step 3 (fill in login form and click on login btn)
url = "https://id-dcr-pre.citroen.com/login?openid.claimed_id=" \
      "http://specs.openid.net/auth/2.0/identifier_select&openid.identity=" \
      "http://specs.openid.net/auth/2.0/identifier_select&openid.mode=checkid_setup&openid.ns=" \
      "http://specs.openid.net/auth/2.0&newcvs=1&goto=" \
      "https://idpcvs-preprod.citroen.com/am/oauth2/authorize?client_id={}&response_type=code&scope=openid%20implied_consent%20profile&redirect_uri=" \
      "https://psa-retail11-citroen-qa.summit-automotive.solutions/digital-api/brandid/login&realm=clientsB2CCitroen&locale=fr-FR&state={}".format(clientId,state)

payload = 'captcha%5Bid%5D=33671f82c3b41aa88bb685230aa042c1&captcha%5Binput%5D=t6n4x8&country=FR' \
          '&csrf_token={}&email=mirowranky22@yopmail.com&formName=indexLogin&lang=fr&' \
          'myProfileHidden=&newcvs=&password=kladivo12345&remember=0&submit=VALIDATE'.format(csrf_token)
headers = {
  'Origin': 'https://id-dcr-pre.citroen.com',
  'Upgrade-Insecure-Requests': '1',
  'Content-Type': 'application/x-www-form-urlencoded',
  'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.80 Safari/537.36',
  'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3',
}

response = s.request("POST", url, headers=headers, data = payload)

print('Request -> {}'.format(url))
print('Query -> {}'.format(payload))
print('Response -> {}'.format(response.status_code))
print('Headers -> {}'.format(response.headers))
print("-"*50)


# step 4
url = "https://idpcvs-preprod.citroen.com/am/oauth2/authorize?client_id={}&response_type=code&scope=openid%20implied_consent%20profile&redirect_uri=" \
      "https://psa-retail11-citroen-qa.summit-automotive.solutions/digital-api/brandid/login&realm=clientsB2CCitroen&locale=fr-FR&state={}".format(clientId,state)

payload = {}
headers = {
  'Origin': 'https://id-dcr-pre.citroen.com',
  'Upgrade-Insecure-Requests': '1',
  'Content-Type': 'application/x-www-form-urlencoded',
  'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.80 Safari/537.36',
  'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3',
}

response = s.request("GET", url, headers=headers, data = payload)

print('Request -> {}'.format(url))
print('Response -> {}'.format(response.status_code))
print('Headers -> {}'.format(response.headers))
print("-"*50)

# step 5
url = "https://psa-retail11-citroen-qa.summit-automotive.solutions/digital-api/brandid/login" \
      "?scope=implied_consent%20openid%20profile&state={}%3D%3D&code={}".format(state,naky_code)

payload = {}
headers = {
  'Origin': 'https://id-dcr-pre.citroen.com',
  'Upgrade-Insecure-Requests': '1',
  'Content-Type': 'application/x-www-form-urlencoded',
  'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.80 Safari/537.36',
  'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3',
  'Cookie': str(final_cookie)
}

response = s.request("GET", url, headers=headers, data = payload)

print('Request -> {}'.format(url))
print('Response -> {}'.format(response.status_code))
print('Response body -> {}'.format(response.text))
print('Headers -> {}'.format(response.headers))
print("-"*50)

# step 6
url = "https://psa-retail11-citroen-qa.summit-automotive.solutions/digital-api/brandid/login" \
      "?scope=implied_consent+openid+profile&state=" \
      "%7B%22redirectTo%22%3A%22https%3A%5C%2F%5C%2Fpsa-retail11-citroen-qa.summit-automotive.solutions%5C%2Fmy-account%22%7D" \
      "&code={}".format(naky_code)

payload = {}
headers = {
  'Origin': 'https://id-dcr-pre.citroen.com',
  'Upgrade-Insecure-Requests': '1',
  'Content-Type': 'application/x-www-form-urlencoded',
  'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.80 Safari/537.36',
  'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3',
  'Cookie': str(final_cookie)
  # 'Cookie': '__cfduid=d9f2bd9aa5aecc156f7b85929b64f7ca21602574395; SAUT_SESSION_AC_preprod=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJqd3QtaXNzdWVyIiwic3ViIjoic3NtLXVzZXItYXV0aCIsIm5iZiI6MTYwNDkyMjgzNCwiY3VzdG9tZXJfaWQiOm51bGwsInNlc3Npb25faWQiOiI3MTJlODNkZi0yMjgyLTExZWItODQ4Yi1jMzVkODg5NDRjYmYiLCJpYXQiOjE2MDQ5MjI4MzQsImp0aSI6IjcxMmU4M2RmLTIyODItMTFlYi04NDhiLWMzNWQ4ODk0NGNiZiJ9.yR8kZIya_tG_SeL0MtwAePAnDNid1iAktQZ_UJ3KpN8; SAUT_SESSION_AC_qa=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJqd3QtaXNzdWVyIiwic3ViIjoic3NtLXVzZXItYXV0aCIsIm5iZiI6MTYwNTA5MTcxMywiY3VzdG9tZXJfaWQiOm51bGwsInNlc3Npb25faWQiOiJhYzkwNDA1NC0yNDBiLTExZWItODQ4Yi1jMzVkODg5NDRjYmYiLCJpYXQiOjE2MDUwOTE3MTMsImp0aSI6ImFjOTA0MDU0LTI0MGItMTFlYi04NDhiLWMzNWQ4ODk0NGNiZiJ9.1iUo8Cks1mYhSpUR8L8uX5wWiy4uSrGUXPxdFYeSBHQ'
}

response = requests.request("GET", url, headers=headers, data = payload)

print('Request -> {}'.format(url))
print('Response -> {}'.format(response.status_code))
print('Response body -> {}'.format(response.text))
print('Headers -> {}'.format(response.headers))
print("-"*50)

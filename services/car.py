import requests

url = "https://store.citroen.co.uk/spc-api/api/v1/gb/en/AC/car-details-list"

payload = "{\"aggregationParams\":{\"levelAggregations\":[{\"name\":\"detailsAggregated\",\"nesting\":[\"detailsAggregated\"],\"children\":[]}],\"relevancyAggregations\":[{\"name\":\"prices.basePrice\",\"fields\":[\n  \"exteriorColour\",\n  \"exteriorColourSlug\",\n  \"engine\",\n  \"specPackSlug\",\n  \"specPack\",\n  \"engineGearboxFuelSlug\",\n  \"gearbox\",\n  \"prices\",\n  \"interiorColour\",\n  \"interiorColourSlug\",\n  \"externalId\",\n  \"nameplateBodyStyleSlug\",\n  \"nameplateBodyStyle\"\n],\"parent\":\"detailsAggregated\",\"operation\":{\"size\":1,\"sort\":\"recommended\"}}]},\"filters\":[{\"nesting\":[\"nameplateBodyStyleSlug\"],\"name\":\"nameplateBodyStyleSlug\",\"operator\":\"EQUALS\",\"value\":\"new-c3-5-door\",\"parent\":null},{\"nesting\":[\"prices\",\"monthlyPrices\",\"amount\"],\"name\":\"prices.monthlyPrices.amount.global\",\"operator\":\"BETWEEN\",\"value\":{\"from\":1,\"to\":99999},\"parent\":null},{\"nesting\":[\"prices\",\"type\"],\"name\":\"prices.type\",\"operator\":\"EQUALS\",\"value\":\"Employee\"},{\"nesting\":[\"stock\"],\"name\":\"stock\",\"operator\":\"EQUALS\",\"value\":\"false\"}],\"extra\":{\"journey\":\"finance\"}}"
# payload = "{\"aggregationParams\":{\"levelAggregations\":[{\"name\":\"detailsAggregated\",\"nesting\":[\"detailsAggregated\"],\"children\":[]}],\"relevancyAggregations\":[{\"name\":\"prices.basePrice\",\"fields\":[\"exteriorColour\",\"engine\",\"specPackSlug\",\"engineGearboxFuelSlug\",\"gearbox\",\"prices\",\"externalId\"],\"parent\":\"detailsAggregated\",\"operation\":{\"size\":1,\"sort\":\"recommended\"}}]},\"filters\":[{\"nesting\":[\"nameplateBodyStyleSlug\"],\"name\":\"nameplateBodyStyleSlug\",\"operator\":\"EQUALS\",\"value\":\"new-c3-5-door\",\"parent\":null},{\"nesting\":[\"prices\",\"monthlyPrices\",\"amount\"],\"name\":\"prices.monthlyPrices.amount.global\",\"operator\":\"BETWEEN\",\"value\":{\"from\":1,\"to\":99999},\"parent\":null},{\"nesting\":[\"prices\",\"type\"],\"name\":\"prices.type\",\"operator\":\"EQUALS\",\"value\":\"Employee\"},{\"nesting\":[\"stock\"],\"name\":\"stock\",\"operator\":\"EQUALS\",\"value\":\"false\"}],\"extra\":{\"journey\":\"finance\"}}"
headers = {
  # 'origin': 'https://store.citroen.co.uk',
  # 'accept-encoding': 'gzip, deflate, br',
  # 'accept-language': 'en-US,en-GB;q=0.9,en;q=0.8',
  # 'x-auth-token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJqd3QtaXNzdWVyIiwic3ViIjoic3NtLXVzZXItYXV0aCIsIm5iZiI6MTYwNjczMDgwMSwiY3VzdG9tZXJfaWQiOm51bGwsInNlc3Npb25faWQiOiJhNDg3NzVjYS0zMmYzLTExZWItODAwOC1iNjI0OTM3NTkyNzkiLCJpYXQiOjE2MDY3MzA4MDEsImp0aSI6ImE0ODc3NWNhLTMyZjMtMTFlYi04MDA4LWI2MjQ5Mzc1OTI3OSJ9.9xTxcjaUyu753gFumCOFddPhpexklwJCaDCYelEj9Cg',
  # 'cookie': '_psac_gdpr_banner_id=0; _psac_gdpr_consent_purposes_opposition=; _psac_gdpr_consent_purposes=[cat_ana][cat_com][cat_soc]; _psac_gdpr_consent_cookies=[Google Tag Manager][Plista][Ozone][IginitionOne (Netmining)][Encore Digital Media][Emetriq][Xaxis][AppNexus][Smart Adserver][Adyoulike][Quantcast][Selligent][Adara][4w MarketPlace][Netmining][mPlatform][Adbrain][Facebook]; _psac_gdpr_consent_given=1; _ga=GA1.3.175222687.1604489896; _gcl_au=1.1.1035045544.1604489896; _hjTLDTest=1; _hjid=eaef7a91-3480-49f5-86e1-a7252b79c35b; _gid=GA1.3.1683957044.1606730681; SAUT_SESSION_SOLUK_AC_prod=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJqd3QtaXNzdWVyIiwic3ViIjoic3NtLXVzZXItYXV0aCIsIm5iZiI6MTYwNjczMDgwMSwiY3VzdG9tZXJfaWQiOm51bGwsInNlc3Npb25faWQiOiJhNDg3NzVjYS0zMmYzLTExZWItODAwOC1iNjI0OTM3NTkyNzkiLCJpYXQiOjE2MDY3MzA4MDEsImp0aSI6ImE0ODc3NWNhLTMyZjMtMTFlYi04MDA4LWI2MjQ5Mzc1OTI3OSJ9.9xTxcjaUyu753gFumCOFddPhpexklwJCaDCYelEj9Cg; _hjIncludedInPageviewSample=1; evo5_popin_instance=1272198; _dc_gtm_UA-46306758-1=1; _dc_gtm_UA-45190795-1=1; _uetsid=76b80dd032f311eb8e63bf3f24913f5a; _uetvid=76b83ec032f311eb814cb9aa2e064dc2',
  # 'pragma': 'no-cache',
  # 'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_16_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.80 Safari/537.36',
  # 'content-type': 'application/json',
  # 'accept': 'application/json',
  # 'cache-control': 'no-cache',
  # 'authority': 'store.citroen.co.uk',
  # 'referer': 'https://store.citroen.co.uk/configurable/finance/'
}

print(type(payload))
response = requests.request("POST", url, data = payload)

print(response.status_code)
